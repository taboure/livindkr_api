<?php

namespace AppBundle\Form;

use Bnbc\UploadBundle\Form\Type\AjaxfileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contenu')
            ->add('titre')
            ->add('image', FileType::class, array(
                'data_class' => null,
                'required' => false
            ))
            ->add('tags')
            ->add('nomAuteur', AjaxfileType)
            ->add('lienBlog')
            ->add('facebook')
            ->add('twitter')
            ->add('instagram');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_article';
    }


}
