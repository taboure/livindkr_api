<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PubliciteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')
            ->add('date_publicite')
            ->add('photo', FileType::class, array(
                "data_class" => null,
                "required" => false
            ))
            ->add('type', ChoiceType::class,    array(
                'placeholder' => 'Choisir l\'emplacement de la publicité',
                'required' => false,
                "choices" => array(
                    'Accueil' => "accueil",
                    'Découverte' => "decouverte",
                    'Prestataires' => 'prestataires'
                )
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Publicite'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_publicite';
    }


}
