<?php

namespace AppBundle\Form;

use Bnbc\UploadBundle\Form\Type\AjaxfileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ThingToDoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('description')
            ->add('adresse')
            ->add('telephone')
            ->add('photo', FileType::class, array(
                "data_class" => null
            ))
            ->add('promo')
            ->add('longitude')
            ->add('latitude')
            ->add('photo', AjaxfileType::class,
                array(
                    'required'            => false,
                    'progressBar'         => true,
                    'progressBarClass'    => 'bnbc-ajax-file-progress',
                    'compound'           => false
                ))
            ->add('offre')
            ->add('categorie')
            ->add('sousCategorie')
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ThingToDo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_thingtodo';
    }


}
