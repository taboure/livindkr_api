<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


        $institutions = $em->getRepository('AppBundle:Institution')->findAll();
        $events = $em->getRepository('AppBundle:Event')->findAll();
        $articles = $em->getRepository('AppBundle:Article')->findAll();
        $publicites = $em->getRepository('AppBundle:Publicite')->findAll();

        return $this->render('AppBundle:Admin:index.html.twig', array(
           'institutions' => $institutions,
            'events' => $events,
            'articles' => $articles,
            'publicites' => $publicites
        ));
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        $em = $this->getDoctrine()->getManager();


        $institutions = $em->getRepository('AppBundle:Institution')->findBy(['user' => $this->getUser()]);
        $events = $em->getRepository('AppBundle:Event')->findBy(['user' => $this->getUser()]);
        $publicites = $em->getRepository('AppBundle:Publicite')->findBy(['user' => $this->getUser()]);

        return $this->render('AppBundle:Admin:dashboard.html.twig', array(
            'institutions' => $institutions,
            'events' => $events,
            'publicites' => $publicites
        ));
    }
}
