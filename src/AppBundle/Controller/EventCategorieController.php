<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EventCategorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Eventcategorie controller.
 *
 * @Route("eventcategorie")
 */
class EventCategorieController extends Controller
{
    /**
     * Lists all eventCategorie entities.
     *
     * @Route("/", name="eventcategorie_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $eventCategories = $em->getRepository('AppBundle:EventCategorie')->findAll();

        return $this->render('eventcategorie/index.html.twig', array(
            'eventCategories' => $eventCategories,
        ));
    }

    /**
     * Creates a new eventCategorie entity.
     *
     * @Route("/new", name="eventcategorie_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $eventCategorie = new Eventcategorie();
        $form = $this->createForm('AppBundle\Form\EventCategorieType', $eventCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($eventCategorie);
            $em->flush();

            return $this->redirectToRoute('eventcategorie_show', array('id' => $eventCategorie->getId()));
        }

        return $this->render('eventcategorie/new.html.twig', array(
            'eventCategorie' => $eventCategorie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a eventCategorie entity.
     *
     * @Route("/{id}", name="eventcategorie_show")
     * @Method("GET")
     */
    public function showAction(EventCategorie $eventCategorie)
    {
        $deleteForm = $this->createDeleteForm($eventCategorie);

        return $this->render('eventcategorie/show.html.twig', array(
            'eventCategorie' => $eventCategorie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing eventCategorie entity.
     *
     * @Route("/{id}/edit", name="eventcategorie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventCategorie $eventCategorie)
    {
        $deleteForm = $this->createDeleteForm($eventCategorie);
        $editForm = $this->createForm('AppBundle\Form\EventCategorieType', $eventCategorie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('eventcategorie_edit', array('id' => $eventCategorie->getId()));
        }

        return $this->render('eventcategorie/edit.html.twig', array(
            'eventCategorie' => $eventCategorie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a eventCategorie entity.
     *
     * @Route("/{id}", name="eventcategorie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, EventCategorie $eventCategorie)
    {
        $form = $this->createDeleteForm($eventCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($eventCategorie);
            $em->flush();
        }

        return $this->redirectToRoute('eventcategorie_index');
    }

    /**
     * Creates a form to delete a eventCategorie entity.
     *
     * @param EventCategorie $eventCategorie The eventCategorie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EventCategorie $eventCategorie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('eventcategorie_delete', array('id' => $eventCategorie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
