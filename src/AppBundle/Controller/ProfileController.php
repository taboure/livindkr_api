<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 24/07/2018
 * Time: 16:18
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProfileController extends BaseController
{
    public function editAction(Request $request)
    {

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $eventDispatcher = $this->get('event_dispatcher');


        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

            if($user->getPhoto()){
                $user->setPhoto($this->_movePhoto($user));
            }

            $this->userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $this->eventDispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    # Traitement de la photo
    protected function _movePhoto($data){
        $fs = new Filesystem();

        $dir = $this->get('kernel')->getRootDir() . '/../web/uploads/';
        $coach_dir = $this->_createEntityFolder($data);

        # On déplace la photo dans le dossier
        if($photo = $data->getPhoto()){
            $ext = pathinfo($photo, PATHINFO_EXTENSION);
            $name = 'photo.' . $ext;

            # Si le fichier existe (il peut ne pas exister dans le cas d'une modification où on uploaderai pas un nouveau fichier)
            if(file_exists($dir . $photo)){
                $fs->copy($dir . $photo, $coach_dir . '/' . $name, true);
                $fs->remove($dir . $photo);
            }
            return $name;
        }
        else {
            return null;
        }
    }

    # Création du dossier pour accueillir l'image
    protected function _createEntityFolder($data){
        $fs = new Filesystem();
        $dir = $this->get('kernel')->getRootDir() . '/../web/data/entity/' . $data->getId();
        try {
            $fs->mkdir($dir);
        }
        catch (IOExceptionInterface $e) {
            echo "Une erreur est survenue lors de la création du dossier : ". $e->getPath();
        }
        return $dir;
    }
}