<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TagDecouverte;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Tagdecouverte controller.
 *
 * @Route("admin/tags")
 */
class TagDecouverteController extends Controller
{
    /**
     * Lists all tagDecouverte entities.
     *
     * @Route("/", name="admin_tags_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tagDecouvertes = $em->getRepository('AppBundle:TagDecouverte')->findAll();

        return $this->render('tagdecouverte/index.html.twig', array(
            'tagDecouvertes' => $tagDecouvertes,
        ));
    }

    /**
     * Creates a new tagDecouverte entity.
     *
     * @Route("/new", name="admin_tags_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tagDecouverte = new Tagdecouverte();
        $form = $this->createForm('AppBundle\Form\TagDecouverteType', $tagDecouverte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($tagDecouverte->getPhoto()){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $tagDecouverte->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $tagDecouverte->setPhoto($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($tagDecouverte);
            $em->flush();

            return $this->redirectToRoute('admin_tags_index');
        }

        return $this->render('tagdecouverte/new.html.twig', array(
            'tagDecouverte' => $tagDecouverte,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tagDecouverte entity.
     *
     * @Route("/{id}", name="admin_tags_show")
     * @Method("GET")
     */
    public function showAction(TagDecouverte $tagDecouverte)
    {
        $deleteForm = $this->createDeleteForm($tagDecouverte);

        return $this->render('tagdecouverte/show.html.twig', array(
            'tagDecouverte' => $tagDecouverte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tagDecouverte entity.
     *
     * @Route("/{id}/edit", name="admin_tags_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TagDecouverte $tagDecouverte)
    {

        $filename = null;

        if ( $tagDecouverte->getPhoto() )
        {
            $filename = $tagDecouverte->getPhoto();
            $tagDecouverte->setPhoto(
                new File($this->getParameter('articles_directory'). '/'. $tagDecouverte->getPhoto())
            );
        }


        $deleteForm = $this->createDeleteForm($tagDecouverte);
        $editForm = $this->createForm('AppBundle\Form\TagDecouverteType', $tagDecouverte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $tagDecouverte->setPhoto($filename) ;
            }

            if($tagDecouverte->getPhoto() && $tagDecouverte->getPhoto() != $filename){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $tagDecouverte->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $tagDecouverte->setPhoto($fileName);
            }


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_tags_edit', array('id' => $tagDecouverte->getId()));
        }

        return $this->render('tagdecouverte/edit.html.twig', array(
            'tagDecouverte' => $tagDecouverte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tagDecouverte entity.
     *
     * @Route("/{id}", name="admin_tags_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TagDecouverte $tagDecouverte)
    {
        $form = $this->createDeleteForm($tagDecouverte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tagDecouverte);
            $em->flush();
        }

        return $this->redirectToRoute('admin_tags_index');
    }

    /**
     * Creates a form to delete a tagDecouverte entity.
     *
     * @param TagDecouverte $tagDecouverte The tagDecouverte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TagDecouverte $tagDecouverte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tags_delete', array('id' => $tagDecouverte->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
