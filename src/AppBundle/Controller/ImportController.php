<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 01/08/2018
 * Time: 15:20
 */

namespace AppBundle\Controller;

use AppBundle\Entity\BienEtre;
use AppBundle\Entity\Hotels;
use AppBundle\Entity\ModeBeaute;
use AppBundle\Entity\NightLife;
use AppBundle\Entity\Prestataires;
use AppBundle\Entity\Restaurants;
use AppBundle\Entity\Shopping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class ImportController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/bienetre", name="import_csv_bienetre")
     * @Method("GET")
     */
    public function importBienetreAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/bienetre.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[0]))
                    {
                        $data_row['nom'] = $data[0];
                    }else{
                        $data_row['nom'] ="";
                    }
                    /*if(isset($data[2]))
                    {
                        $data_row['categorie'] = $data[2];
                    }else{
                        $data_row['categorie'] ="";
                    }*/
                    if(isset($data[2]))
                    {
                        $data_row['souscategorie'] = $data[2];
                    }else{
                        $data_row['souscategorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['telephone'] = $data[3];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['adresse'] = $data[4];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['email'] = $data[5];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['coordonnees'] = $data[6];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    /*if(isset($data[8]))
                    {
                        $data_row['tags'] = $data[8];
                    }else{
                        $data_row['tags'] ="";
                    }*/
                    if(isset($data[7]))
                    {
                        $data_row['date_visite'] = $data[7];
                    }else{
                        $data_row['date_visite'] ="";
                    }
                    if(isset($data[8]))
                    {
                        $data_row['pack'] = $data[8];
                    }else{
                        $data_row['pack'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['horraires'] = $data[9];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[10]))
                    {
                        $data_row['description'] = $data[10];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[11]))
                    {
                        $data_row['price'] = $data[11];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[12]))
                    {
                        $data_row['siteweb'] = $data[12];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[13]))
                    {
                        $data_row['facebook'] = $data[13];
                    }else{
                        $data_row['facebook'] = "";
                    }
                    if(isset($data[14]))
                    {
                        $data_row['instagram'] = $data[14];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 2 && $i <= 30 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new BienEtre();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(2));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                if(utf8_encode($v['souscategorie']) == "Sport et Fitness") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(9));
                }elseif (utf8_encode($v['souscategorie']) == "Soins") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(8));
                }


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }


    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/hotels", name="import_hotels_csv")
     * @Method("GET")
     */
    public function importHotelsAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/hotel.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[0]))
                    {
                        $data_row['nom'] = $data[0];
                    }else{
                        $data_row['nom'] ="";
                    }
                    /*if(isset($data[2]))
                    {
                        $data_row['categorie'] = $data[2];
                    }else{
                        $data_row['categorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['souscategorie'] = $data[3];
                    }else{
                        $data_row['souscategorie'] ="";
                    }*/
                    if(isset($data[1]))
                    {
                        $data_row['telephone'] = $data[1];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[2]))
                    {
                        $data_row['adresse'] = $data[2];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['email'] = $data[3];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['coordonnees'] = $data[4];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    /*if(isset($data[8]))
                    {
                        $data_row['tags'] = $data[8];
                    }else{
                        $data_row['tags'] ="";
                    }*/
                    if(isset($data[5]))
                    {
                        $data_row['horraires'] = $data[6];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[8]))
                    {
                        $data_row['pack'] = $data[8];
                    }else{
                        $data_row['pack'] ="";
                    }
                    if(isset($data[7]))
                    {
                        $data_row['description'] = $data[7];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[8]))
                    {
                        $data_row['price'] = $data[8];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['siteweb'] = $data[9];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[10]))
                    {
                        $data_row['facebook'] = $data[10];
                    }else{
                        $data_row['facebook'] = "";
                    }

                    if(isset($data[11]))
                    {
                        $data_row['instagram'] = $data[11];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    if(isset($data[12]))
                    {
                        $data_row['twitter'] = $data[12];
                    }else{
                        $data_row['twitter'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 1 && $i <= 172 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new Hotels();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(14));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }




    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/modebeaute", name="import_modebeaute_csv")
     * @Method("GET")
     */
    public function importModeBeauteAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/modebeaute.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[0]))
                    {
                        $data_row['nom'] = $data[0];
                    }else{
                        $data_row['nom'] ="";
                    }
                    /*if(isset($data[2]))
                    {
                        $data_row['categorie'] = $data[2];
                    }else{
                        $data_row['categorie'] ="";
                    }
                    */
                    if(isset($data[2]))
                    {
                        $data_row['souscategorie'] = $data[2];
                    }else{
                        $data_row['souscategorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['telephone'] = $data[3];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['adresse'] = $data[4];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['email'] = $data[5];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['coordonnees'] = $data[6];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    /*if(isset($data[8]))
                    {
                        $data_row['tags'] = $data[8];
                    }else{
                        $data_row['tags'] ="";
                    }*/
                    if(isset($data[7]))
                    {
                        $data_row['horraires'] = $data[7];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['pack'] = $data[9];
                    }else{
                        $data_row['pack'] ="";
                    }
                    /*if(isset($data[5]))
                    {
                        $data_row['horraires'] = $data[5];
                    }else{
                        $data_row['horraires'] ="";
                    }*/
                    if(isset($data[10]))
                    {
                        $data_row['description'] = $data[10];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[11]))
                    {
                        $data_row['price'] = $data[11];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[12]))
                    {
                        $data_row['siteweb'] = $data[12];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[13]))
                    {
                        $data_row['facebook'] = $data[13];
                    }else{
                        $data_row['facebook'] = "";
                    }
                    if(isset($data[14]))
                    {
                        $data_row['instagram'] = $data[14];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 2 && $i <= 74 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new ModeBeaute();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(3));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                if(utf8_encode($v['souscategorie']) == "Esthetique") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(6));
                }elseif (utf8_encode($v['souscategorie']) == "Couture") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(7));
                }


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }



    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/nightlife", name="import_nightlife_csv")
     * @Method("GET")
     */
    public function importNightlifeAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/nightlife.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[1]))
                    {
                        $data_row['nom'] = $data[1];
                    }else{
                        $data_row['nom'] ="";
                    }
                    /*if(isset($data[2]))
                    {
                        $data_row['categorie'] = $data[2];
                    }else{
                        $data_row['categorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['souscategorie'] = $data[3];
                    }else{
                        $data_row['souscategorie'] ="";
                    }*/
                    if(isset($data[3]))
                    {
                        $data_row['telephone'] = $data[3];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['adresse'] = $data[4];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['email'] = $data[5];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['coordonnees'] = $data[6];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    /*if(isset($data[8]))
                    {
                        $data_row['tags'] = $data[8];
                    }else{
                        $data_row['tags'] ="";
                    }*/
                    if(isset($data[7]))
                    {
                        $data_row['horraires'] = $data[7];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['pack'] = $data[9];
                    }else{
                        $data_row['pack'] ="";
                    }
                    /*if(isset($data[5]))
                    {
                        $data_row['horraires'] = $data[5];
                    }else{
                        $data_row['horraires'] ="";
                    }*/
                    if(isset($data[10]))
                    {
                        $data_row['description'] = $data[10];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[11]))
                    {
                        $data_row['price'] = $data[11];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[12]))
                    {
                        $data_row['siteweb'] = $data[12];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[13]))
                    {
                        $data_row['facebook'] = $data[13];
                    }else{
                        $data_row['facebook'] = "";
                    }
                    if(isset($data[14]))
                    {
                        $data_row['instagram'] = $data[14];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 2 && $i <= 26 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new NightLife();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(4));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }


    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/prestataires", name="import_csv_prestataires")
     * @Method("GET")
     */
    public function importPrestatairesAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/prestataire.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[0]))
                    {
                        $data_row['nom'] = $data[2] .' '. $data[3];
                    }else{
                        $data_row['nom'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['telephone'] = $data[4];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['adresse'] = $data[5];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['email'] = $data[6];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[7]))
                    {
                        $data_row['siteweb'] = $data[7];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 3 && $i <= 227 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new Prestataires();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));


                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(5));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }

    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/restaurants", name="import_csv")
     * @Method("GET")
     */
    public function importRestaurantsAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/restaurant.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[1]))
                    {
                        $data_row['nom'] = $data[1];
                    }else{
                        $data_row['nom'] ="";
                    }
                    if(isset($data[2]))
                    {
                        $data_row['categorie'] = $data[2];
                    }else{
                        $data_row['categorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['souscategorie'] = $data[3];
                    }else{
                        $data_row['souscategorie'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['telephone'] = $data[4];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['adresse'] = $data[5];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['email'] = $data[6];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[7]))
                    {
                        $data_row['coordonnees'] = $data[7];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    if(isset($data[8]))
                    {
                        $data_row['tags'] = $data[8];
                    }else{
                        $data_row['tags'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['horraires'] = $data[9];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[10]))
                    {
                        $data_row['pack'] = $data[10];
                    }else{
                        $data_row['pack'] ="";
                    }

                    if(isset($data[12]))
                    {
                        $data_row['description'] = $data[12];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[13]))
                    {
                        $data_row['price'] = $data[13];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[14]))
                    {
                        $data_row['siteweb'] = $data[14];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[15]))
                    {
                        $data_row['facebook'] = $data[15];
                    }else{
                        $data_row['facebook'] = "";
                    }
                    if(isset($data[16]))
                    {
                        $data_row['instagram'] = $data[16];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 2 && $i <= 227 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new Restaurants();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(1));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                if(utf8_encode($v['souscategorie']) == "Fast Food") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(1));
                }elseif (utf8_encode($v['souscategorie']) == "Patisseries/Glaciers") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(2));
                }
                elseif (utf8_encode($v['souscategorie']) == "Restaurants") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(14));
                }


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }


    /**
     * Lists all article entities.
     *
     * @Route("/import/etablissements/shopping", name="import_csv_shopping")
     * @Method("GET")
     */
    public function importShoppingAction(Request $request)
    {
        // dump($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath()); die;

        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $etablissements = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        if (($handle = fopen($baseurl. "/csv/shopping.csv", "r")) !== FALSE) { // Lecture du fichier, à adapter
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée

                // dump($data); die;
                $row++;
                $data_row = array();
                for ($c = 0; $c < $num; $c++) {
                    if(isset($data[0]))
                    {
                        $data_row['nom'] = $data[0];
                    }else{
                        $data_row['nom'] ="";
                    }
                    if(isset($data[1]))
                    {
                        $data_row['categorie'] = $data[1];
                    }else{
                        $data_row['categorie'] ="";
                    }
                    if(isset($data[2]))
                    {
                        $data_row['souscategorie'] = $data[2];
                    }else{
                        $data_row['souscategorie'] ="";
                    }
                    if(isset($data[3]))
                    {
                        $data_row['telephone'] = $data[3];
                    }else{
                        $data_row['telephone'] ="";
                    }
                    if(isset($data[4]))
                    {
                        $data_row['adresse'] = $data[4];
                    }else{
                        $data_row['adresse'] ="";
                    }
                    if(isset($data[5]))
                    {
                        $data_row['email'] = $data[5];
                    }else{
                        $data_row['email'] ="";
                    }
                    if(isset($data[6]))
                    {
                        $data_row['coordonnees'] = $data[6];
                    }else{
                        $data_row['coordonnees'] ="";
                    }
                    if(isset($data[9]))
                    {
                        $data_row['horraires'] = $data[9];
                    }else{
                        $data_row['horraires'] ="";
                    }
                    if(isset($data[8]))
                    {
                        $data_row['pack'] = $data[8];
                    }else{
                        $data_row['pack'] ="";
                    }

                    if(isset($data[10]))
                    {
                        $data_row['description'] = $data[10];
                    }else{
                        $data_row['description'] ="";
                    }
                    if(isset($data[11]))
                    {
                        $data_row['price'] = $data[11];
                    }else{
                        $data_row['price'] ="";
                    }
                    if(isset($data[12]))
                    {
                        $data_row['siteweb'] = $data[12];
                    }else{
                        $data_row['siteweb'] = "";
                    }
                    if(isset($data[13]))
                    {
                        $data_row['facebook'] = $data[13];
                    }else{
                        $data_row['facebook'] = "";
                    }
                    if(isset($data[14]))
                    {
                        $data_row['instagram'] = $data[14];
                    }else{
                        $data_row['instagram'] ="";
                    }
                    $etablissements[$row] = $data_row;
                }
            }
            fclose($handle);

        }

        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        $i = 1;

        // dump($etablissements); die;

        foreach ($etablissements as $v) {

            if($i >= 2 && $i <= 227 )
            {
                // dump($v);
                // On crée un objet utilisateur
                $restaurant = new Shopping();

                $restaurant->setNom(utf8_encode($v['nom']));
                $restaurant->setTelephone(utf8_encode($v['telephone']));
                $restaurant->setAdresse(utf8_encode($v['adresse']));
                $restaurant->setEmail(utf8_encode($v['email']));
                $restaurant->setHoraires($data_row['horraires']) ;

                $coordonnees = explode(',', $v['coordonnees']);
                if(isset($coordonnees[0]))
                    $restaurant->setLatitude((float)$coordonnees[0]);
                if(isset($coordonnees[1]))
                    $restaurant->setLongitude((float)$coordonnees[1]);

                $restaurant->setPrice(utf8_encode($v['price']));
                $restaurant->setSiteweb(utf8_encode($v['siteweb']));
                $restaurant->setFacebook(utf8_encode($v['facebook']));
                $restaurant->setInstagram(utf8_encode($v['instagram']));
                $restaurant->setStatus(1);

                $restaurant->setCategorie($em->getRepository('AppBundle:Categorie')->find(6));
                $restaurant->setUser($em->getRepository('AppBundle:User')->find(1));
                $restaurant->setOffre($em->getRepository('AppBundle:Offre')->find(2));


                if(utf8_encode($v['souscategorie']) == "Habillement") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(3));
                }elseif (utf8_encode($v['souscategorie']) == "Specialty Shops") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(12));
                }
                elseif (utf8_encode($v['souscategorie']) == "Chaussures et accessoires") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(4));
                }
                elseif (utf8_encode($v['souscategorie']) == "Bijouterie") {
                    $restaurant->setSousCategorie($em->getRepository('AppBundle:SousCategorie')->find(5));
                }


                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($restaurant);
                // Ecriture dans la base de données
                $em->flush();
            }

            $i++;
        }



        // Renvoi la réponse (ici affiche un simple OK pour l'exemple)
        return new Response('OK');
    }
}