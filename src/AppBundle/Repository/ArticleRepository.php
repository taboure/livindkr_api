<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 03/09/2018
 * Time: 13:17
 */

namespace AppBundle\Repository;


class ArticleRepository
{
    public function findAll()
    {
        return $this->findBy(array());
    }
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $persister = $this->_em->getUnitOfWork()->getEntityPersister($this->_entityName);

        return $persister->loadAll($criteria, $orderBy, $limit, $offset);
    }

}