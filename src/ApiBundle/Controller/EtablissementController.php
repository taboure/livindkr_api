<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 05/09/2018
 * Time: 17:32
 */

namespace ApiBundle\Controller;


use AppBundle\Entity\Note;
use AppBundle\Entity\Institution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use AppBundle\Entity\Categorie;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\QueryBuilder;


class EtablissementController extends Controller
{
    // Tous ce qui concerne les établissements
    /**
     * @Rest\View()
     * @Rest\Get("/api/etablissements")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne tous les etablissements de livin dkr"
     * )
     */
    public function getEtablissement() {
        $etablissements = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Institution')
            ->findBy(array('status' => true));

        $formatted = [];
        foreach ($etablissements as $etablissement) {
            if($etablissement->getCategorie()->getIdentifiant() == "restaurants") {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'menus' => $etablissement->getMenus(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto(),
                    'tags' => $etablissement->getEtiquettes()

                ] ;
            } else {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto(),
                    'tags' => $etablissement->getEtiquettes()

                ] ;
            }
        }

        return $formatted;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/categories/etablissements")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne tous les etablissements de livin dkr suivant une catégorie"
     * )
     */
    public function getEtablissementByCategory($id) {

        $category = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Categorie')
            ->find($id);

        $etablissements = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Institution')
            ->findBy(array('categorie' => $category,'status' => true));

        $formatted = [];



        foreach ($etablissements as $etablissement) {
            if($etablissement->getCategorie()->getIdentifiant() == "restaurants") {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'menus' => $etablissement->getMenus(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto()

                ] ;
            } else {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto()

                ] ;
            }
        }

        return $formatted;
    }


    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/souscategories/etablissements")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne tous les etablissements de livin dkr suivant une sous catégorie"
     * )
     */
    public function getEtablissementBySousCategory($id) {

        $sous_category = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:SousCategorie')
            ->find($id);

        $etablissements = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Institution')
            ->findBy(array('sousCategorie' => $sous_category, 'status' => true));

        $formatted = [];
        foreach ($etablissements as $etablissement) {
            if($etablissement->getCategorie()->getIdentifiant() == "restaurants") {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorieFomatted(),
                    'promo' => $etablissement->getPromo(),
                    'menus' => $etablissement->getMenus(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto()

                ] ;
            } else {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'avis' => $etablissement->getAvis(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto()

                ] ;
            }
        }

        return $formatted;
    }


    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/etablissements")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne un etablissement de livin dkr suivant son identifiant"
     * )
     */
    public function getEtablissementById($id) {

        $etablissement = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        $formatted = [];

        if($etablissement){
            if($etablissement->getCategorie()->getIdentifiant() == "restaurants") {

            } else {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorieFomatted(),
                    'promo' => $etablissement->getPromo(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),
                    'offre' => $etablissement->getOffre(),
                    'photo' => $etablissement->getPhoto()

                ] ;
            }
        }else{
            $formatted = [
                "message" => "Cet etablissement ne figure pas dans la plateforme"
            ];
        }

        return $formatted;
    }

}